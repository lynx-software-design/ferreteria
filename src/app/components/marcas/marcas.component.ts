import { Component, OnInit } from '@angular/core';
import marcas from '../../../assets/data/marcas.json';

@Component({
  selector: 'app-marcas',
  templateUrl: './marcas.component.html',
  styleUrls: ['./marcas.component.css']
})
export class MarcasComponent implements OnInit {
  marcaslist = marcas.list

  constructor() { }

  ngOnInit(): void {
  }

}
